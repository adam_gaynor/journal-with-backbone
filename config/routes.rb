Rails.application.routes.draw do
  root to: 'roots#root'
  resources :posts, :defaults => { :format => :json }
end
