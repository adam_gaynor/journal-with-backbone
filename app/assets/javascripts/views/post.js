Journal.Views.Post = Backbone.View.extend({

  template: JST["posts/post"],

  tagName: 'ul',

  initialize: function () {
    this.listenTo(this.model, "sync", this.render)
  },

  events: {
    "click .post-delete": "handleItemDelete",
    "dblclick .title": "handleEditTitle",
    "dblclick .body": "handleEditBody",
    "blur .title": "submitNewTitle",
    "blur .body": "submitNewBody"
  },

  render: function () {
    this.$el.html(this.template({ post: this.model } ));

    return this;
  },

  handleItemDelete: function () {
    this.model.destroy();
    Backbone.history.navigate('hello', { trigger: true } );
  },

  handleEditTitle: function () {
    $target = $(event.currentTarget);
    var $titleInput = $("<input>");
    $titleInput.attr('autofocus', true);
    $titleInput.val($target.find(".title").text());
    $target.find(".title").empty();
    $target.find(".title").html($titleInput);
  },

  submitNewTitle: function () {
    var newTitle = $(event.target).val();

    this.model.save({"title": newTitle});
  },

  handleEditBody: function () {
    $target = $(event.currentTarget);
    var $bodyInput = $("<textarea>");
    $bodyInput.attr('autofocus', true);
    $bodyInput.val($target.find(".body").text());
    $target.find(".body").empty();
    $target.find(".body").html($bodyInput);
  },

  submitNewBody: function () {
    var newBody = $(event.target).val();
    this.model.save({"body": newBody});
  }

})
