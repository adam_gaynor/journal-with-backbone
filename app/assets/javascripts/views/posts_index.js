Journal.Views.PostsIndex = Backbone.View.extend({
  initialize: function() {
    this.listenTo(this.collection, "sync reset remove", this.render)
  },

  template: JST["posts/index"],

  tagName: "ul",

  refreshPosts: function () {
    this.collection.fetch({reset: true});
  },

  render: function () {
    this.$el.empty();
    var index = this;

    this.collection.models.forEach(function (post) {
      var postItemIndexView = new Journal.Views.PostsIndexItem ({
        model: post
      });

      index.$el.append(postItemIndexView.render().$el);
    })

    var $newForm = $('<a>');
    $newForm.attr('href', '/#/posts/new');
    $newForm.text('New Post');
    this.$el.append('<br>');
    this.$el.append($newForm);

    return this;
  }

})
