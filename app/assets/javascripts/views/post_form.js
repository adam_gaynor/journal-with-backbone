Journal.Views.PostForm = Backbone.View.extend({
  template: JST['posts/post_form'],

  initialize: function () {
    if (! this.collection) {
      this.collection = Journal.Collections.posts;
    }
  },

  events: {
    "submit .post-form": "submitForm"
  },

  render: function () {
    this.$el.html(this.template({ post: this.model } ));

    return this;
  },
  submitForm: function (event) {
    event.preventDefault();

    var formData = $(event.currentTarget).serializeJSON();

    this.model.save(formData, {
      success: function () {
        this.collection.add(this.model);

        var targetFragment = '/posts/' + this.model.id
        Backbone.history.navigate(targetFragment, {
          trigger: true
        })
      }.bind(this)
    })
  }
})
