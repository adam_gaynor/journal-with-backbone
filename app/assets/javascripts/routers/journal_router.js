Journal.Routers.JournalRouter = Backbone.Router.extend({
  routes: {
    "hello": "root",
    "/": "postsIndex",
    "posts/new": "postsNew",
    "posts/:id": "postsShow",
    "posts/:id/edit": "postsEdit"
  },

  initialize: function () {
    this.postsIndex();
    this.$rootEl = $('.main-content');
  },

  root: function() {
    this._currentView && this._currentView.remove();
    this._currentView = null;
  },

  postsIndex: function () {
    Journal.Collections.posts.fetch();

    var indexView = new Journal.Views.PostsIndex({
      collection: Journal.Collections.posts
      });
    $('.sidebar').html(indexView.render().$el);
  },

  postsShow: function (id) {
    var post = Journal.Collections.posts.getOrFetch(id);
    var postView = new Journal.Views.Post({model: post});
    this._swapView(postView);
  },

  postsEdit: function (id) {
    var post = Journal.Collections.posts.getOrFetch(id);
    var editView = new Journal.Views.PostForm({model: post});
    this._swapView(editView);
  },

  postsNew: function() {
    var post = new Journal.Models.Post();
    var newPostView = new Journal.Views.PostForm({
      model: post,
      collection: Journal.Collections.posts
    });
    this._swapView(newPostView);

  },

  _swapView: function (newView) {
    this._currentView && this._currentView.remove();
    this._currentView = newView;
    this.$rootEl.html(newView.$el);
    newView.render();
  }

});
